#.rst
# GangaTools
# ----------
#
# Define functions to enable integration of a project with Ganga.
#

CMAKE_MINIMUM_REQUIRED(VERSION 3.0.0)

set(GANGA_BINARY_DIR ${CMAKE_BINARY_DIR}/ganga
    CACHE PATH "Working directory for building project distribution kit for Ganga.")
set(GANGA_INPUT_SANDBOX_FILE ${GANGA_BINARY_DIR}/input-sandbox.tgz
    CACHE FILEPATH "Filename for Ganga input sandbox file.")
mark_as_advanced(GANGA_BINARY_DIR GANGA_INPUT_SANDBOX_FILE)

function(ganga_create_job_runner)
  file(MAKE_DIRECTORY ${GANGA_BINARY_DIR})

  # scan build.conf (if present) and prepare the corresponding options for lb-run
  if(EXISTS ${CMAKE_SOURCE_DIR}/build.conf)
    file(STRINGS ${CMAKE_SOURCE_DIR}/build.conf build_conf_lines)
    foreach(l ${build_conf_lines})
      if(l MATCHES "nightly_base=.+")
        string(REPLACE "nightly_base=" "--nightly-base " nightly_base_opt "${l}")
      elseif(l MATCHES "nightly_day=.+")
        string(REPLACE "nightly_day=" "" nightly_day "${l}")
      elseif(l MATCHES "nightly_slot=.+")
        string(REPLACE "nightly_slot=" "--nightly " nightly_slot_opt "${l}")
      endif()
    endforeach()
    if(nightly_slot_opt AND nightly_day)
      set(nightly_slot_opt "${nightly_slot_opt} ${nightly_day}")
    endif()
  endif()

  file(WRITE ${GANGA_BINARY_DIR}/run
       "#!/bin/sh
exec lb-run ${nightly_base_opt} ${nightly_slot_opt} --user-area \$(cd \$(dirname \$0) && pwd) --use-grid ${CMAKE_PROJECT_NAME}/${CMAKE_PROJECT_VERSION} \"$@\"
")
  if(UNIX)
    execute_process(COMMAND chmod 755 ${GANGA_BINARY_DIR}/run)
  endif()
endfunction()

function(ganga_input_sandbox)
  set(dist_base_dir ${GANGA_BINARY_DIR}/${CMAKE_PROJECT_NAME}_${CMAKE_PROJECT_VERSION})

  add_custom_target(ganga-clean-install
                    COMMAND ${CMAKE_COMMAND} -E remove_directory ${CMAKE_INSTALL_PREFIX}
                    COMMAND ${CMAKE_MAKE_PROGRAM} install
                    COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_INSTALL_PREFIX}/python
                    COMMAND ${CMAKE_MAKE_PROGRAM} post-install
                    COMMENT "Preparing InstallArea for input sandbox")

  set(copy_sources)
  foreach(src CMakeLists.txt cmt ${packages})
    if(EXISTS ${CMAKE_SOURCE_DIR}/${src})
      if(IS_DIRECTORY ${CMAKE_SOURCE_DIR}/${src})
        set(copy_sources ${copy_sources}
            COMMAND ${CMAKE_COMMAND} -E copy_directory ${src} ${dist_base_dir}/${src})
      else()
        set(copy_sources ${copy_sources}
            COMMAND ${CMAKE_COMMAND} -E copy ${src} ${dist_base_dir}/${src})
      endif()
    endif()
  endforeach()
  add_custom_target(ganga-dist-prepare
                    COMMAND ${CMAKE_COMMAND} -E remove_directory ${dist_base_dir}
                    ${copy_sources}
                    COMMAND ${CMAKE_COMMAND} -E copy_directory InstallArea/${BINARY_TAG} ${dist_base_dir}/InstallArea/${BINARY_TAG}
                    WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
                    DEPENDS ganga-clean-install)

  add_custom_target(ganga-input-sandbox
                    COMMAND ${CMAKE_COMMAND} -E remove -f ${GANGA_INPUT_SANDBOX_FILE}
                    COMMAND ${CMAKE_COMMAND} -E tar cfz ${GANGA_INPUT_SANDBOX_FILE}
                            run ${CMAKE_PROJECT_NAME}_${CMAKE_PROJECT_VERSION}
                    WORKING_DIRECTORY ${GANGA_BINARY_DIR}
                    DEPENDS ganga-dist-prepare
                    COMMENT "Preparing input sandbox tarball")
endfunction()

function(enable_ganga_integration)
  ganga_create_job_runner()
  ganga_input_sandbox()
endfunction()
