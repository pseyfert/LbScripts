###############################################################################
# (c) Copyright 2016 CERN                                                     #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''

Test of the 

Created on Jun 7, 2016

@author: Ben Couturier
'''
import logging
import os
import sys
import unittest
from os.path import normpath, join


projectConfigTestJson = '''
{
    "heptools": {
        "binary_tag": "$CMTCONFIG",
        "packages": [
            [
                "Python",
                "2.7.9.p1",
                [
                    "bin"
                ]
            ],
            [
                "pygraphics",
                "1.5_python2.7",
                [
                    "bin",
                    "python",
                    "lib"
                ]
            ],
            [
                "qt",
                "4.8.4",
                [
                    "bin",
                    "python",
                    "lib"
                ]
            ],
            [
                "gcc",
                "4.9.3",
                [
                    "bin",
                    "python",
                    "lib"
                ]
            ],
            [
                "mysql",
                "5.5.27",
                [
                    "bin",
                    "python",
                    "lib"
                ]
            ],
            [
                "pytools",
                "1.9_python2.7",
                [
                    "bin",
                    "python"
                ],
		[
		    ["PYTOOLS_TEST", "${PYTOOLS_HOME}/test"]
		]
            ]
        ],
        "version": 84
    },
    "name": "LHCbDirac",
    "used_projects": {
        "project": [
            [
                "Dirac",
                "v0r0",
                [
                    "bin",
                    "python",
                    "lib"
                ]
            ],
            [
                "LHCbGrid",
                "v10r0",
                [
                    "bin",
                    "python",
                    "lib"
                ]
            ]
        ]
    },
    "version": "v8r3"
}

'''

class Test(unittest.TestCase):
    ''' Test case of the AppImporter CMakelists parser '''

    def setUp(self):
        ''' Setup the test '''
        self._python_dir = normpath(join(*([os.path.dirname(__file__), os.pardir, 'python' ])))
        sys.path.insert(0, self._python_dir)
        logging.basicConfig()
        import LbUtils.Log
        LbUtils.Log._default_log_format = '%(asctime)s:' \
                                          + LbUtils.Log._default_log_format

    def tearDown(self):
        ''' Tear down the test '''
        pass

    def testAddvariable(self):
        from LbUtils.LbRunConfigTools import prettify
        from LbUtils.LbRunConfigTools import ManifestGenerator, XEnvGenerator
        import json
        config = json.loads(projectConfigTestJson)
        xg = XEnvGenerator( config )
        xe = xg.getDocument()
        logging.warning( prettify( xe ))

        # Now checking that we have PYTOOLS_HOME
        pytools_home = [ c for c in xe if  c.tag == 'env:set' and c.attrib['variable'] == 'PYTOOLS_HOME' ]
        self.assertEqual(len(pytools_home), 1)

        # And set PYTOOLS_TEST to the correct value
        pytools_test = [ c for c in xe if  c.tag == 'env:set' and c.attrib['variable'] == 'PYTOOLS_TEST' ]
        self.assertEqual(len(pytools_test), 1)
        self.assertEqual(pytools_test[0].text, "${PYTOOLS_HOME}/test")
        
if __name__ == "__main__":
    unittest.main()
