#!/bin/sh
# cern-lxdistcc-wrappers.sh: v1 KELEMEN Peter <lxdistcc-admins@cern.ch>
#
# 2012-10-17: Modified by Marco Clemencic
#
_self=${0##*/}

hostos() {
    arch=$(uname -i)
    case $(lsb_release -si) in
        ScientificCERNSLC)
            os=slc
            vers=$(lsb_release -sr | cut -d. -f1)
            ;;
        *)
            os=$(lsb_release -si | tr '[:upper:]' '[:lower:]')
            vers=$(lsb_release -sr)
    esac
    echo $arch-$os$vers
}

set_prefix() {
    _dirname=$1
    _version=$2

    _platform=${LCG_hostos:-$(hostos)}

    for _prefix in $(echo ${LCG_release_area} ${LCG_external_area} | tr : ' ') \
                   /cvmfs/sft.cern.ch/lcg/{contrib,external,releases} \
                   /afs/cern.ch/sw/lcg/{contrib,releases,external,experimental} ; do
        if [ -d "${_prefix}/${_dirname}/${_version}/${_platform}" ] ; then
            _prefix="${_prefix}/${_dirname}/${_version}/${_platform}"
            break
        fi
    done
}

setup_gcc() {
        _version=$1

        set_prefix gcc ${_version}

        _bin="${_prefix}/bin"
        _lib="${_prefix}/lib64"
        LD_LIBRARY_PATH=$(echo $LD_LIBRARY_PATH | sed 's-[^:]*/gcc/[^:]*:\?--g')
        LD_LIBRARY_PATH="${_lib}${LD_LIBRARY_PATH:+:}${LD_LIBRARY_PATH}"
        PATH="${_bin}${PATH:+:}${PATH}"
        COMPILER_PATH="${_prefix}/lib/gcc/x86_64-unknown-linux-gnu/${_version}"

        GCC_TOOLCHAIN="${_prefix}"
        export LD_LIBRARY_PATH
        export PATH
        export COMPILER_PATH
        export GCC_TOOLCHAIN
}

setup_clang() {

        _clang_version=$1
        case ${_clang_version} in
          3.2) setup_gcc 4.6.3 ;;
          3.3|3.4) setup_gcc 4.8.1 ;;
          *) setup_gcc 4.9.3 ;;
        esac

        set_prefix llvm ${_clang_version}

        _bin="${_prefix}/bin"
        _lib="${_prefix}/lib64"
        LD_LIBRARY_PATH=$(echo $LD_LIBRARY_PATH | sed 's-[^:]*/llvm/[^:]*:\?--g')
        LD_LIBRARY_PATH="${_lib}${LD_LIBRARY_PATH:+:}${LD_LIBRARY_PATH}"
        PATH="${_bin}${PATH:+:}${PATH}"

        export LD_LIBRARY_PATH
        export PATH
        export COMPILER_PATH
}

case ${_self} in

        lcg-[cg]++-[0-9].[0-9].[0-9]|lcg-gcc-[0-9].[0-9].[0-9]|lcg-gfortran-[0-9].[0-9].[0-9])

                _version=${_self##*-}
                setup_gcc $_version
                _self=${_self%-*}
                _self=${_self#*-}
                ;;

        lcg-clang-*|lcg-clang++-*)

                _version=${_self##*-}
                setup_clang $_version
                _self=${_self%-*}
                _self=${_self#*-}
                if [ "${_self}" = "clang" -o "${_self}" = "clang++" ] ; then
                  _self="${_self} --gcc-toolchain=${GCC_TOOLCHAIN}"
                fi
                ;;

        *)
                echo "E: Unsupported compiler '${_self}', please contact <marco.clemencic@cern.ch>"
                exit 100
                ;;
esac
exec ${_bin}/${_self} "$@"

# End of file.
