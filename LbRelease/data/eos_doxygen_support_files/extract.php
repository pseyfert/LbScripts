<?php
$zip = $_GET['zip'];
$path = $_GET['path'];

if ( ! $zip || ! file_exists($zip) ) {
  header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found', true, 404);
  return;
}

switch(pathinfo($path, PATHINFO_EXTENSION)) {
  case 'css':  $ct = 'text/css';         break;
  case 'png':  $ct = 'image/png';        break;
  case 'gif':  $ct = 'image/gif';        break;
  case 'jpg':  $ct = 'image/jpeg';       break;
  case 'js':   $ct = 'text/javascript';  break;
  case 'svg':  $ct = 'image/svg+xml';    break;
  case 'json': $ct = 'application/json'; break;
  default:     $ct = 'text/html';
}
header('Content-Type: ' . $ct);

passthru('/usr/bin/unzip -p "' . $zip . '" "' . $path. '"');
?>
