#!/usr/bin/env python
"""
@author: Marco Clemencic <marco.clemencic@cern.ch>
"""
import sys
import os
import re
import shutil
import json
import logging
import urllib
from datetime import datetime, timedelta
from subprocess import Popen, PIPE, call
from socket import gethostname
from LbConfiguration.External import doxygen_version as doxygen_default_version
from LbUtils import which as _which
from LbConfiguration.SP2 import lookup
from LbConfiguration.SP2.version import isValidVersion, versionKey
from LbConfiguration.Project import project_names as _project_names

#--- Configuration information utils

# Prepare a dictionary mapping the project name in upper case to the
# actual case
PROJECT_NAMES = dict((_p.upper(), _p) for _p in _project_names)

# they must be uppercase to match Doc.projects
LCG_NAMES = set(('LCGCMT', 'LCG', 'HEPTOOLS'))


def projectURL(project, version=None):
    """
    Return the URL to the web page of the project.
    """
    result = None
    project = project.upper()
    if project in LCG_NAMES:
        result = 'http://lcgsoft.web.cern.ch/lcgsoft'
        if version:
            result += '/release/' + version
    else:
        if project == "GAUDI":
            result = "http://cern.ch/gaudi"
        else:
            result = "http://cern.ch/LHCb-release-area/DOC/{0}".format(
                project.lower()
            )
        if version:
            result += "/releases/" + version
    return result

#--- Filesystem utils


_has_kinit = bool(_which('kinit'))

DOCS_HTACCESS_HEAD = '''# GENERATED FILE: DO NOT EDIT
Options -Indexes
RewriteEngine On
# this is a clever trick for avoiding RewriteBase
# see http://stackoverflow.com/a/21063276
RewriteCond "%{REQUEST_URI}::$1" "^(.*?/)(.*)::\\2$"
RewriteRule "^(.*)$" "-" [E=BASE:%1]
# add /index.html to the top entry
RewriteRule "^([^/]+/[^/]+)/?$" "%{ENV:BASE}$1/index.html" [R,L]
# redirect to the content of the right .zip file
'''
DOCS_HTACCESS_LINE = ('RewriteRule "^{pv}/(.+)" '
                      '"%{{ENV:BASE}}../{doc}/$1" [PT,L]\n')
DOCS_HTACCESS_LATEST_LINE = ('RewriteRule "^{p}/latest/(.+)" '
                             '"%{{ENV:BASE}}{p}/{v}/$1" [PT,L]\n')

# FIXME: make DocsDB a context manager
class DocsDB(object):
    '''
    Database of project/version -> doc directory
    '''
    def __init__(self, root):
        '''
        Load database from JSON file.
        '''
        self.path = os.path.join(root, 'docs')
        try:
            with open(os.path.join(self.path, 'db.json'), 'rb') as f:
                self.data = json.load(f)
        except IOError as err:
            if err.errno == 2:
                self.data = {}
            else:
                raise
        self._dirty = False
    def referenced(self):
        '''
        Return name of Doc dirs known.
        '''
        return set(self.data.values())
    def write(self):
        '''
        Write current db content to JSON file.
        '''
        if self._dirty:
            if not os.path.isdir(self.path):
                os.makedirs(self.path)
            with open(os.path.join(self.path, 'db.json'), 'wb') as f:
                json.dump(self.data, f, indent=2)
            with open(os.path.join(self.path, '.htaccess'), 'w') as f:
                f.write(DOCS_HTACCESS_HEAD)
                entries = sorted(self.data, key=versionKey)
                f.writelines(DOCS_HTACCESS_LINE.format(pv=pv,
                                                       doc=self.data[pv])
                             for pv in entries)
                latest_versions = dict(pv.split('/') for pv in entries)
                f.write('# redirect special version "latest"\n')
                f.writelines(DOCS_HTACCESS_LATEST_LINE.format(p=p, v=v)
                             for p, v in latest_versions.items())
            self._dirty = False
    def _key(self, project, version):
        return '{0}/{1}'.format(project.lower(), version)
    def add(self, project, version, doc):
        self._dirty = True
        self.data[self._key(project, version)] = doc
    def remove(self, project, version):
        try:
            del self.data[self._key(project, version)]
            self._dirty = True
        except KeyError:
            pass
    def get(self, project, version):
        return self.data.get(self._key(project, version))



#--- Doxygen class


class DoxyFileCfg(object):
    """
    Dictionary-like class to simplify the generation of doxygen configuration
    files.

    This class implements the basic dictionary interface with a record of the
    order in which the elements where entered.

    The conversion to string generates the content for the doxyfile.cfg file.
    """

    def __init__(self, data=None):
        """
        Initialized the object (by default empty).

        @param data: an optional association list (list of key/value pairs)
        """
        self._keys = []
        self._data = {}
        if data is not None:
            for k, v in data:
                self[k] = v

    def __getitem__(self, key):
        """
        Dictionary interface.
        """
        return self._data[key]

    def __setitem__(self, key, value):
        """
        Dictionary interface.
        """
        if key not in self._data:
            self._keys.append(key)
        self._data[key] = value

    def __delitem__(self, key):
        """
        Dictionary interface.
        """
        del self._data[key]
        del self._keys[self._keys.index(key)]

    def keys(self):
        """
        Dictionary interface.
        """
        return self._keys

    def __iter__(self):
        """
        Dictionary interface.
        """
        return self._keys.__iter__()

    def _convertValue(self, value):
        """
        Convert a value to the corresponding doxygen string and return it.
        E.g. bool -> YES/NO, list -> space separated lists
        """
        t = type(value)
        if t is bool:
            if value:
                value = "YES"
            else:
                value = "NO"
        elif t is str:
            if " " in value:
                value = '"%s"' % value
        elif t in (list, tuple):
            value = " ".join(map(self._convertValue, value))
        elif value is None:
            value = ""
        else:
            value = str(value)
        return value

    def __repr__(self):
        """
        Representation of the object instance.
        """
        return "DoxyFileCfg(%r)" % [(k, self[k]) for k in self]

    def __str__(self):
        """
        Produce the doxyfile.cfg content.
        """
        return "".join(["%s = %s\n" % (k, self._convertValue(self[k]))
                        for k in self])


def doxyTagsToDBM(tag, output, overwrite=False, python=False):
    """
    Extract the file paths of the reference documentation page of the classes
    from a Doxygen tag file and stores them in a DBM file.

    @param tag: path to the tag file
    @param output: path to the DBM file.
    @param overwrite: if set to True, force the creation of a new DB (default is append)
    @param python: if set to True, modify the class names to use '.' instead of '::'
    """
    if "LHCBDOC_TESTING" in os.environ:
        return
    import anydbm
    from xml.etree.ElementTree import parse
    # We need to analyse structures like:
    # <compound kind="class">
    #   <name>MyClass</name>
    #   <filename>d1/d70/class_my_class.html</filename>
    #   <member kind="variable" protection="private">
    #     <type>string</type>
    #     <name>m_data</name>
    #     <anchorfile>...</anchorfile>
    #     <anchor>...</anchor>
    #     <arglist></arglist>
    #   </member>
    # </compound>
    tags = parse(tag)
    # Open (and create) the database
    db = anydbm.open(output, overwrite and "n" or "c")
    db.update((el.find('name').text.replace('::', '.' if python else '::'),
               el.find('filename').text)
              for el in tags.iterfind('compound[@kind="class"]'))
    db.close()


def doxyTagsToJSON(tag, output, python=False):
    """
    Extract the file paths of the reference documentation page of the classes
    from a Doxygen tag file and stores them in a JSON file.

    @param tag: path to the tag file
    @param output: path to the JSON file.
    @param python: if set to True, modify the class names to use '.' instead of '::'
    """
    if "LHCBDOC_TESTING" in os.environ:
        return
    from xml.etree.ElementTree import parse
    # We need to analyse structures like:
    # <compound kind="class">
    #   <name>MyClass</name>
    #   <filename>d1/d70/class_my_class.html</filename>
    #   <member kind="variable" protection="private">
    #     <type>string</type>
    #     <name>m_data</name>
    #     <anchorfile>...</anchorfile>
    #     <anchor>...</anchor>
    #     <arglist></arglist>
    #   </member>
    # </compound>
    tags = parse(tag)
    data = dict((el.find('name').text.replace('::', '.' if python else '::'),
                 el.find('filename').text)
                for el in tags.iterfind('compound[@kind="class"]'))
    with open(output, 'wb') as out:
        json.dump(data, out, indent=2)


def _getProjDepsCMT(projroot, recursive=False):
    """
    Get all the dependencies of a project.

    @param projroot: root directory of the project

    @return: list of pairs (project, version)
    """
    cmtdir = os.path.join(projroot, "cmt")
    logging.debug("running 'cmt show projects' in %s", cmtdir)
    # fine tuning the environment to please CMT
    env = dict(os.environ)
    env['PWD'] = cmtdir
    env['CMTPROJECTPATH'] = ':'.join(lookup.path)
    cmt = Popen(['cmt', 'show', 'projects'], cwd=cmtdir, env=env,
                stdout=PIPE, stderr=PIPE)
    out, err = cmt.communicate()
    for l in err.splitlines():
        match = re.search(
            r'Warning: Project ([A-Z0-9]+) [A-Z0-9]+_([vrpg0-9]+).*not found', l)
        if match:
            raise lookup.MissingProjectError(match.group(1), match.group(2),
                                             'any platform', lookup.path)

    lines = out.splitlines()[1:]  # skip first line (this project)
    # note: in non recursive mode we require only 2 spaces in front
    deps = set((l[0], l[1].split("_")[1])
               for l in [l.strip().split()
                         for l in lines
                         if recursive or l[2] != ' ']
               if l and l[1] != "(in")
    return deps


def _getProjDeps(project, version, recursive=False):
    """
    Get all the dependencies of a project.

    @param project: name of the project
    @param version: version of the project

    @return: list of pairs (project, version)
    """
    all_deps = set()
    if project.lower() == 'lbscripts':  # LbScripts shows some deps to ignore
        return all_deps
    for _, _, deps in lookup.walkProjectDeps(project, version):
        deps[:] = set(deps).difference(all_deps)
        all_deps.update(deps)
        if not recursive:
            break
    if not all_deps:
        logging.debug('no dependencies, trying with CMT')
        proj_root = lookup.findProject(project, version, 'any')
        if os.path.exists(os.path.join(proj_root, 'cmt', 'project.cmt')):
            all_deps = _getProjDepsCMT(proj_root, recursive)
        else:
            logging.debug('not a CMT project, assuming no dependencies')
    return all_deps


def findPackages(projdir):
    '''
    Return a generator for the package names from the specified project directory.
    '''
    for root, dirs, files in os.walk(projdir):
        if 'InstallArea' in dirs:
            dirs.remove('InstallArea')
        if root == projdir:
            continue  # no need to check the top level
        if 'CMakeLists.txt' in files or 'cmt' in dirs:
            # this is a package
            dirs[:] = []  # no need to recurse further
            yield os.path.relpath(root, projdir)


def findFilesInSubdirs(path):
    '''
    Return a generator for all the files in a directory and its subdirs.
    The path to the files is relative to the top directory passed as argument.
    '''
    for root, _, files in os.walk(path):
        for filename in files:
            yield os.path.relpath(os.path.join(root, filename), path)

MAIN_PAGE_HEADER = r'''/** \mainpage LHCb Software Documentation
\htmlonly
<p style="text-align:center;font-style:italic;font-size:120%%;font-weight:bold;">%(subtitle)s</p>
\endhtmlonly
Documentation for the projects:
'''
MAIN_PAGE_FOOTER = r'''
\htmlonly
<div align="center">
<p>Graph of the dependencies between projects</p>
<object data="dependencies.svg" type="image/svg+xml"></object>
</div>
\endhtmlonly
*/
'''
#--- Documentation class


class Doc(object):

    """
    Class to describe and manipulate documentation directory.

    A couple of special files can be used to tune the behavior of the documentation
    directory:
     '.locked' : prevent the build of the documentation (used internally to mark if a
                 documentation is being built)
     '==REBUILD==' : force a re-build of the documentation
    """
    _nameRegexp = re.compile("^DOC_[0-9]{6}$")
    _namePattern = "DOC_%06d"
    _docLockFile = ".locked"
    _docBlacklistFile = "blacklist.txt"
    _docRebuildFlagFile = "==REBUILD=="
    _docLockTimeFmt = "%Y-%m-%dT%H:%M:%S.%f"  # same as datetime.isoformat()

    @classmethod
    def makeNew(cls, root):
        '''
        Generate a new Doc directory in the root path.
        '''
        logging.debug("New documentation dir requested")
        # get all doc names
        docs = cls._allDocNames(root)
        if not docs:
            n = 0
        else:
            # convert the number at the end of the last doc name
            # to a number and increment it by 1
            id_n = docs[-1].split("_")[-1].lstrip("0")
            if id_n:
                n = int(id_n) + 1
            else:
                # this happens when the latest is "DOC_000000"
                n = 1
        name = cls._namePattern % n
        logging.debug("Automatic directory name %s", name)
        return Doc(os.path.join(root, name))

    def __init__(self, path):
        """
        Initialize the object.
        If the directory doesn't exist it is created.

        @param name: name of the documentation directory, if not specified, a
                     new one is created
        @param root: base directory of the documentations, by default taken
                     from the environment variable DOCROOT
        """
        # full path to the directory of the object
        self.path = path
        # base directory of the documentation directories and name name of this
        self.root, self.name = os.path.split(path)
        self.zipname = os.path.join(self.path, self.name + '.zip')

        # get our logger instance
        self._log = logging.getLogger(self.name)

        # Broken links in the directory
        self.broken = []

        # projects in the directory
        self.projects = {}
        if not os.path.isdir(self.path):
            self._log.info("Creating directory %s", self.path)
            os.makedirs(self.path)
        else:
            self._log.debug("Listing projects in %s", self.path)
            for l in [s for s in os.listdir(self.path)
                      if not s.startswith('.')]:
                if os.path.islink(os.path.join(self.path, l)):
                    p, v = l.split("_")
                    # ensure that we use upper case names
                    self.projects[p.upper()] = v
                    # Update broken links count
                    if not os.path.exists(os.path.join(self.path, l)):
                        self.broken.append(l)
            self._log.debug("Found %d projects: %s", len(self.projects),
                            ", ".join(map(str, sorted(self.projects.items()))))

        # flag to tell if the DOC dir is locked or not
        self._lockFile = os.path.join(self.path, self._docLockFile)
        if os.path.exists(self._lockFile):
            pid, time = open(self._lockFile).read().strip().split(" - ", 1)
            self.locked = (
                int(pid), datetime.strptime(time, "%Y-%m-%dT%H:%M:%S.%f"))
        else:
            self.locked = None

        self._rebuildFlagFile = os.path.join(
            self.path, self._docRebuildFlagFile)
        # flag saying if the directory has already been built or not
        self.toBeBuilt = not os.path.exists(self.zipname)
        if os.path.exists(self._rebuildFlagFile):
            self._log.warning("Re-build requested")
            self.toBeBuilt = True

        # get the list of projects that should not be used in this slot
        self._blackListFile = os.path.join(self.path, self._docBlacklistFile)
        try:
            self.blackList = set(x.upper() for x in
                                 open(self._blackListFile).read().split())
        except IOError:
            self.blackList = set()

        # check that the common links are present
        # a missing link means problems during the build
        db = DocsDB(self.root)
        for project, version in self.projects.items():
            if not db.get(project, version):
                self._log.warning(
                    "Project %s %s in %s but not in DocsDB",
                    project, version, self.name)

    @classmethod
    def _allDocNames(cls, root):
        """
        Get all doc names in the same top-level directory as this one
        (including it).
        """
        logging.debug("Looking for documentation directories in %s", root)
        docs = [d for d in filter(cls._nameRegexp.match, os.listdir(root))
                if os.path.isdir(os.path.join(root, d))]
        docs.sort()
        return docs

    @classmethod
    def all(cls, root):
        '''
        Get all the doc objects for a root directory.
        '''
        return [Doc(os.path.join(root, name))
                for name in cls._allDocNames(root)]

    def _hasCpp(self):
        """
        Tell if we have to build the C++ documentation (i.e. if Gaudi is in the
        dependencies.
        """
        return "GAUDI" in self.projects

    def getVersion(self, project):
        """
        Return the version of the project contained, or None if the project is
        not present.
        """
        project = project.upper()
        return self.projects.get(project, None)

    def canHost(self, project, version, deps):
        """
        Helper function to tell if a project with its dependencies can be
        included in this directory.
        """
        # Note: if the directory is locked, we can assume we could still host the
        #       file, but it will not be actually added

        to_add = set([p.upper() for p, _ in deps])
        to_add.add(project.upper())

        # Projects without dependencies (like LbScripts) cannot be added to any
        # existing documentation.
        if not deps:
            return False
        # The project must be there with the right version or not at all
        if self.getVersion(project) not in (version, None):
            return False
        # The project should not be blacklisted
        if self.blackList.intersection(to_add):
            return False
        # Other special case: LHCbDirac cannot go in the same doc dir as Gaudi.
        # If at least one the projects to be added (project+deps) is in the
        # conflicting list (first intersection), ensure that the one are not
        # (second intersection) among the others (difference).
        conflicts = set(["GAUDI", "LHCBDIRAC"])
        if conflicts.intersection(to_add):
            if conflicts.difference(to_add).intersection(self.projects):
                return False
        # Each dependency must be already present with the right version or
        # not present and there must be projects in the dependencies that are
        # already in the doc
        has_common = False
        for p, v in deps:
            hosted = self.getVersion(p)
            if hosted is not None:  # we do have the project p
                if v == hosted:  # with the same version
                    has_common = True
                else:  # with another version
                    return False
        return has_common

    def add(self, project, version):
        """
        Add a project to the list of contained ones.

        Raise and exception if the project is already there with a different
        version or it doesn't exist.
        """
        # do not add projects if the directory is locked
        if self.locked:
            self._log.warning("Trying to add project %s %s to a locked "
                              "directory", project, version)
            return
        project = project.upper()  # ensure upper case
        # check if the project is already there
        if project in self.projects:
            if self.getVersion(project) != version:
                raise ValueError("Project %s already in %s" %
                                 (project, self.name))
        else:
            self._log.info("Adding project %s %s", project, version)
            # make link to the root directory of the project in the doc dir
            try:
                root = lookup.findProject(project, version, 'any')
                self.projects[project] = version
                self._log.info("found at %s", root)
                os.symlink(root, os.path.join(self.path,
                                              "%s_%s" % (project, version)))
                # self._log.debug("Mark the directory as to be built")
                self.toBeBuilt = True
            except lookup.MissingProjectError as err:
                self._log.warning(str(err))

    def _generateDoxygenMainPage(self):
        """
        Generate the main page file for the documentation directory and return
        it as a string.

        @param depgraphsize: tuple with the width and height of the dependency
            graph (to give the correct size to the svg image).
        """
        page = MAIN_PAGE_HEADER + '<ul>\n'
        projects = self.projects.keys()
        projects.sort()

        item = '<a href="%(homeurl)s">%(project)s</a> <a href="%(versurl)s">%(version)s</a>'
        for p in projects:
            if p in LCG_NAMES:
                continue
            page += "<li>%s</li>\n" % (
                item % {
                    # get actual case of the project
                    'project': PROJECT_NAMES.get(p.upper(), p),
                    # url to the project web page
                    'homeurl': projectURL(p),
                    # version of the project
                    'version': self.getVersion(p),
                    # url to the version of the project
                    'versurl': projectURL(p, self.getVersion(p)),
                }
            )
        page += "</ul>\n"
        if LCG_NAMES.intersection(self.projects):
            lcg_name = list(LCG_NAMES.intersection(self.projects))[0]
            page += "Based on %s.\n" % (
                item % {
                    'project': 'LCG',
                    'homeurl': projectURL('LCG'),
                    'version': self.getVersion(lcg_name),
                    'versurl': projectURL('LCG', self.getVersion(lcg_name))
                }
            )
        # page += '\n\\image html dependencies.png "Graph of the dependencies
        # between projects"\n'
        page += MAIN_PAGE_FOOTER
        return page

    def _generateDoxyConf(self, doxygen_versions=(None, None)):
        """
        Generate the doxygen configuration file for the current doc directory.
        """
        if self.locked:
            self._log.warning(
                "Cannot generate Doxygen configuration in a locked directory")
            return
        self._log.info("Generate Doxygen configuration")
        # Get the doxygen versions from the arguments, using the default if not
        # defined
        cpp_version, py_version = [x or doxygen_default_version
                                   for x in doxygen_versions]
        if cpp_version != py_version:
            self._log.warning(
                "Different versions of Doxygen for C++ and Python. "
                "Generating configuration for %s", cpp_version)

        # prepare config directory
        confdir = os.path.join(self.path, "conf")
        if not os.path.isdir(confdir):
            self._log.debug("Creating directory %s", confdir)
            os.makedirs(confdir)

        # FIXME: doxygen tagfiles for externals are not provided anymore,
        #        so no need to fix this yet
        # get externals versions (used for the tag files)
        gaudipath = os.path.join(self.path,
                                 "GAUDI_%s" % self.getVersion("GAUDI"),
                                 "GaudiRelease", "cmt")
        if os.path.isdir(gaudipath):  # we use Gaudi as entry point, if it
                                      # is not there, no external versions
            gaudipath = os.path.join(self.path,
                                     "GAUDI_%s" % self.getVersion("GAUDI"),
                                     "GaudiRelease", "cmt")
            # Translate the cmt output
            #  LCG_config_version='55b'
            #  COOL_config_version='COOL_2_6_0'
            #  CORAL_config_version='CORAL_2_1_0'
            #  POOL_config_version='POOL_2_8_1'
            # into a dictionary
            #  { 'LCG': '55b',
            #    'COOL': 'COOL_2_6_0',
            #    'CORAL': 'CORAL_2_1_0',
            #    'POOL': 'POOL_2_8_1' }
            config_versions = dict(
                [(k.replace("_config_version", ""), v.strip("'"))
                 for k, v in [l.split("=")
                              for l in Popen(["cmt", "show", "macros",
                                              "_config_version"],
                                             cwd=gaudipath, stdout=PIPE)
                              .communicate()[0].splitlines()
                             ]
                ])
        else:
            config_versions = {}

        doxycfg = DoxyFileCfg()
        # The order of the configuration options matches (more or less) the one
        # in the manual: http://www.doxygen.org/config.html
        #--- Project related options
        doxycfg["PROJECT_NAME"] = "LHCb Software"
        doxycfg['OUTPUT_DIRECTORY'] = os.path.join(self.path, 'doxygen')
        doxycfg['CREATE_SUBDIRS'] = True
        doxycfg['CASE_SENSE_NAMES'] = False
        doxycfg['JAVADOC_AUTOBRIEF'] = True
        doxycfg['BUILTIN_STL_SUPPORT'] = True

        #--- Build related options
        doxycfg['EXTRACT_ALL'] = True
        # append the commands to document also private and static members
        doxycfg["EXTRACT_PRIVATE"] = True
        doxycfg["EXTRACT_STATIC"] = True
        doxycfg["EXTRACT_LOCAL_CLASSES"] = True
        doxycfg['GENERATE_TODOLIST'] = True

        #--- Optimizations
        doxycfg['LOOKUP_CACHE_SIZE'] = 4

        #--- Options related to warning and progress messages
        doxycfg['QUIET'] = False
        doxycfg['WARNINGS'] = True
        doxycfg['WARN_LOGFILE'] = "DoxyWarnings.log"

        #--- Input related options
        project_dirs = ["%s_%s" % item
                        for item in self.projects.items()
                        if item[0] not in LCG_NAMES]  # avoid some projects
        # find a binary dir common to all projects
        configs = None
        for p in project_dirs:
            inst_area = os.path.join(self.path, p, 'InstallArea')
            if os.path.exists(inst_area):
                proj_configs = set(c for c in os.listdir(inst_area)
                                   if not c.startswith('.'))
                if configs is None:
                    configs = proj_configs
                else:
                    configs.intersection_update(proj_configs)
        if configs:
            config = configs.pop()  # let's pick one up at random
        else:
            # it's unlikely that we end up with no suitable config,
            # but we have to cover the case
            config = None

        # add all subdirs of the projects, but only the include from
        # InstallArea
        inputs = []
        excludes = []
        for p in project_dirs:
            self._log.debug('project: %s', p)
            # find all packages and add them to the search path
            packs = set(findPackages(os.path.join(self.path, p)))
            inputs.extend(os.path.join(p, subdir) for subdir in packs)
            self._log.debug('%d packages: %s', len(packs), ', '.join(packs))

            if config:
                inc_dir = os.path.join(p, 'InstallArea', config, 'include')
                if os.path.exists(os.path.join(self.path, inc_dir)):
                    self._log.debug('looking for includes in %s', inc_dir)
                    # add InstallArea include dir to the search path
                    inputs.append(inc_dir)
                    # but we need to remove the duplicates from the various
                    # packages
                    installed_headers = set(
                        findFilesInSubdirs(os.path.join(self.path, inc_dir)))
                    # for each package, exclude from the files in the install area that
                    # are already in the package
                    for pack in packs:
                        pack_files = set(
                            findFilesInSubdirs(os.path.join(self.path, p,
                                                            pack)))
                        to_exclude = installed_headers.intersection(pack_files)
                        if to_exclude:
                            self._log.debug('except %d files found in %s',
                                            len(to_exclude), pack)
                            excludes.extend(os.path.join('*', inc_dir, f)
                                            for f in to_exclude)

        doxycfg['INPUT'] = inputs

        doxycfg['RECURSIVE'] = True
        excludes += [
            "*/dict/*",
            # Exclude tests
            "*/test/*",
            "*/Test/*",
            "*/tests/*",
            "*/Tests/*",
            "*/utest/*",
            "*/examples/*",
            "*/GaudiExamples/*",
            # Exclude binaries
            "*/slc3_*/*",
            "*/slc4_*/*",
            "*/*-slc5-*/*",
            "*/win32_*/*",
            "*/*-winxp-*/*",
            "*/osx105_*/*",
            "*/*-mac106-*/*",
            # Exclude version control metadata
            "*/CVS/*",
            "*/.svn/*",
            # Exclude some generated files
            "*/html/*",
            #"*/genConf/*",
            # Exclude problematic files
            "*/doc/MainPage.h",
            "*/Panoramix/doc/doxygen/*",
            "*/Panoramix/doc/h/*",
            "*/qcustomplot.cpp",
            "*/Gen/LbAmpGen/src/*",
        ]
        doxycfg["EXCLUDE_PATTERNS"] = excludes
        files = []
        for p in doxycfg["INPUT"]:
            if ((p.endswith("Sys") and not p.endswith("GaudiSys")) or
                    p.endswith("Release")):
                # FILE_PATTERNS   += *LHCB_<VERSION>/LHCbSys*requirements
                files.append("*%s*requirements" % p)
        doxycfg["FILE_PATTERNS"] = files
        doxycfg['LAYOUT_FILE'] = os.path.join("conf", "DoxygenLayout.xml")
        doxycfg['IMAGE_PATH'] = ["conf"]

        #--- Source browsing related options
        doxycfg['SOURCE_BROWSER'] = True
        doxycfg['INLINE_SOURCES'] = True

        #--- Alphabetical index options
        doxycfg['ALPHABETICAL_INDEX'] = True
        doxycfg['COLS_IN_ALPHA_INDEX'] = 3

        #--- HTML related options
        doxycfg['GENERATE_HTML'] = True
        doxycfg['HTML_TIMESTAMP'] = True
        doxycfg['SEARCHENGINE'] = True
        doxycfg['SERVER_BASED_SEARCH'] = True
        # doxycfg['GENERATE_ECLIPSEHELP']= True
        # doxycfg['ECLIPSE_DOC_ID']      = self.name

        #--- LaTeX related options
        # (useful also for formulas)
        doxycfg['GENERATE_LATEX'] = False
        # doxycfg['LATEX_BATCHMODE']     = True  # I prefer the \nonstopmode
        doxycfg["EXTRA_PACKAGES"] = ["times", "amsmath"]

        #--- RTF related options
        doxycfg['GENERATE_RTF'] = False

        #--- Man page related options
        doxycfg['GENERATE_MAN'] = False

        #--- Preprocessor related options
        doxycfg['SKIP_FUNCTION_MACROS'] = True

        #--- External reference options
        # doxycfg['GENERATE_TAGFILE'] # set to the temporary directory
        doxycfg["TAGFILES"] = []
        # libstdc++
        cppref_tagfile = os.path.join(confdir,
                                      'cppreference-doxygen-web.tag.xml')
        if os.path.exists(cppref_tagfile):
            os.remove(cppref_tagfile)
        urllib.urlretrieve('http://upload.cppreference.com/mwiki/images/f/f8/'
                           'cppreference-doxygen-web.tag.xml', cppref_tagfile)
        doxycfg["TAGFILES"].append(
            '{0}={1}'.format(cppref_tagfile, 'http://en.cppreference.com/w/'))
        # add tag entries for the Application Area projects
        for aa_project in ["ROOT", "CORAL", "COOL", "POOL"]:
            if aa_project in config_versions:
                tagline = os.path.join(os.environ["LCG_release_area"],
                                       aa_project, config_versions[aa_project],
                                       "doc", "doxygen", "html",
                                       aa_project + "_reference_tags.xml")
                tagline += (
                    "=http://lcgapp.cern.ch/doxygen/%s/%s/doxygen/html" %
                    (aa_project, config_versions[aa_project])
                )
                doxycfg["TAGFILES"].append(tagline)
        # doxycfg["TAGFILES"] = []

        #--- Dot options
        doxycfg['CLASS_DIAGRAMS'] = True
        doxycfg["HAVE_DOT"] = True
        doxycfg["DOT_NUM_THREADS"] = 1  # doxygen 1.7
        doxycfg["CLASS_GRAPH"] = True
        doxycfg["COLLABORATION_GRAPH"] = True
        doxycfg["INCLUDE_GRAPH"] = True
        doxycfg["INCLUDED_BY_GRAPH"] = True
        doxycfg["GRAPHICAL_HIERARCHY"] = True
        doxycfg["DOT_PATH"] = None

        # Write the output files

        # Special manipulation required for the C++ and Python versions
        orig = {}
        for k in ['FILE_PATTERNS', 'WARN_LOGFILE', 'INPUT']:  # keep a copy
            orig[k] = doxycfg[k]

        if self._hasCpp():
            # C++ configuration
            doxycfg['FILE_PATTERNS'] = [
                "*.h", "*.icpp", "*.cpp"] + orig['FILE_PATTERNS']
            doxycfg['WARN_LOGFILE'] = orig[
                'WARN_LOGFILE'].replace(".log", "Cpp.log")
            doxycfg['INPUT'] = orig['INPUT'] + ["conf/MainPageCpp.doxygen"]
            open(os.path.join(confdir, "DoxyFileCpp.cfg"), "w").write(
                str(doxycfg))

        # Python configuration
        doxycfg['FILE_PATTERNS'] = ["*.py"] + orig['FILE_PATTERNS']
        doxycfg['WARN_LOGFILE'] = orig[
            'WARN_LOGFILE'].replace(".log", "Py.log")
        doxycfg['INPUT'] = orig['INPUT'] + ["conf/MainPagePy.doxygen"]
        # see http://www.doxygen.org/config.html#cfg_optimize_output_java
        doxycfg["OPTIMIZE_OUTPUT_JAVA"] = True
        open(os.path.join(confdir, "DoxyFilePy.cfg"), "w").write(str(doxycfg))

        # Create the auxiliary files in the conf directory
        from LbRelease._LHCbDocResources import getString
        # generate the dependency graph
        self._genDepGraph(confdir)
        mp_data = self._generateDoxygenMainPage()
        mp_subtitle = '%s Code Version. <a href="%s/index.html">Go to %s</a>'
        if self._hasCpp():
            with open(os.path.join(confdir, "MainPageCpp.doxygen"), "w") as f:
                f.write(mp_data %
                        {"subtitle": mp_subtitle % ("C++", "py", "Python")})
            with open(os.path.join(confdir, "MainPagePy.doxygen"), "w") as f:
                f.write(mp_data %
                        {"subtitle": mp_subtitle % ("Python", "..", "C++")})
        else:
            with open(os.path.join(confdir, "MainPagePy.doxygen"), "w") as f:
                f.write(mp_data % {"subtitle": ""})
        # layout file
        with open(os.path.join(confdir, 'DoxygenLayout.xml'), 'w') as f:
            f.write(getString('layout.xml', cpp_version))
        # class locator PHP script
        with open(os.path.join(confdir, 'class.php'), 'w') as f:
            f.write(getString('class.php', cpp_version))

    def _projectDeps(self, project, recursive=False):
        """
        Return the list of projects a project depend on.
        If the flag 'recursive' is set to True, return the complete (recursive)
        set of dependencies.
        """
        return set(n.upper()
                   for n, _ in _getProjDeps(project, self.getVersion(project),
                                            recursive))

    def _genDepGraph(self, destination=""):
        """
        Generate the project dependency graph.

        @param destination: optional destination path of the generated files,
            relative to the documentation directory or absolute.
        """
        self._log.info(
            "Generating project dependency graph in '%s'", destination)
        dotdata = []
        for project in self.projects:
            project = PROJECT_NAMES.get(project.upper(), project)
            deps = [PROJECT_NAMES.get(p.upper(), p)
                    for p in self._projectDeps(project)
                    if p.upper() != 'LCG' or project.upper() in
                    ('GAUDI', 'GEANT4')]
            deps.sort()
            dotdata.append("%s->{%s};" % (project, ";".join(deps)))
        cmd = ["dot"]
        for format_name in ["png", "svg", "eps", "fig"]:
            if isinstance(format_name, tuple):
                format_name, extension = format_name
            else:
                extension = format_name
            cmd.append("-T%s" % format_name)
            cmd.append("-o%s" % os.path.join(self.path, destination,
                                             "dependencies.%s" % extension))
        dotfile = os.path.join(self.path, destination, "dependencies.dot")
        cmd.append(dotfile)
        with open(dotfile, "w") as f:
            f.write(("digraph dependencies {\nbgcolor=transparent;"
                     "\nnode [fontsize=10,style=filled,fillcolor=white];\n"
                     "%s\n}\n" % ("\n".join(dotdata))) if dotdata else "")
        call(cmd)

    def _buildDox(self, conf, workdir, version=None):
        """
        Run Doxygen using a configuration file to generate the output into a
        specified directory.

        @param conf: path to the configuration file
        @param workdir: output directory for Doxygen (override configuration)
        @param version: version of Doxygen to use
        """
        retcode = 0
        if "LHCBDOC_TESTING" not in os.environ:
            # use a temporary configuration file to generate the output in the
            # work directory
            import tempfile
            tmpFd, tmpName = tempfile.mkstemp(prefix=conf)
            try:
                tmp = os.fdopen(tmpFd, "w")
                tmp.write(open(conf).read())
                tmp.write("\nOUTPUT_DIRECTORY = %s\n" % workdir)
                tmp.write("\nGENERATE_TAGFILE = %s\n" %
                          os.path.join(workdir, "html", "doxygen.tag"))
                tmp.write("\nWARN_LOGFILE = %s\n" %
                          os.path.join(workdir, 'DoxyWarnings.log'))
                tmp.close()
                doxcmd = ["doxygen"]
                if version:
                    doxcmd += ["--doxygen-version", version]
                doxcmd.append(tmpName)
                proc = Popen(doxcmd, cwd=self.path, stdin=PIPE)
                proc.stdin.write(
                    "r\n")  # make latex enter \nonstopmode on the first error
                retcode = proc.wait()
            finally:
                os.remove(tmpName)
        else:
            # Fake execution for testing
            os.mkdir(os.path.join(workdir, "html"))
            open(os.path.join(workdir, "html", "index.html"), "w").write(
                "testing\n")

        if retcode != 0:
            raise RuntimeError(
                "Doxygen failed with error %d in %s" % (retcode, workdir))

    def _buildCpp(self, workdir, doxygen_version=None):
        """
        Build the actual doxygen documentation (C++).

        @param doxygen_version: version of Doxygen to use
        """
        self._buildDox(os.path.join(self.path, "conf", "DoxyFileCpp.cfg"),
                       workdir, version=doxygen_version)
        self._log.info("Generate the database of classes from tags")
        doxyTagsToDBM(os.path.join(workdir, "html", "doxygen.tag"),
                      os.path.join(workdir, "html", "classes.db"))
        doxyTagsToJSON(os.path.join(workdir, "html", "doxygen.tag"),
                       os.path.join(workdir, "html", "classes.json"))

    def _buildPy(self, workdir, doxygen_version=None):
        """
        Build the actual doxygen documentation (Python).

        @param doxygen_version: version of Doxygen to use
        """
        self._buildDox(os.path.join(self.path, "conf", "DoxyFilePy.cfg"),
                       workdir, version=doxygen_version)
        self._log.info("Generate the database of classes from tags")
        doxyTagsToDBM(os.path.join(workdir, "html", "doxygen.tag"),
                      os.path.join(workdir, "html", "classes.db"),
                      python=True)
        doxyTagsToJSON(os.path.join(workdir, "html", "doxygen.tag"),
                       os.path.join(workdir, "html", "classes.json"),
                       python=True)

    def build(self, doxygen_versions=(None, None)):
        """
        Build the doxygen documentation.
        Prepare the infrastructure to build both C++ and Python documentation,
        then call the specific methods.

        @param doxygen_versions: pair of strings defining the version of
                                 doxygen to be used for C++ and Python
        """
        metadata = {'host': gethostname(),
                    'pid': os.getpid(),
                    'started': datetime.now().strftime(self._docLockTimeFmt),
                    'projects': [{'name': PROJECT_NAMES.get(p.upper(), p),
                                  'version': v}
                                 for p, v in sorted(self.projects.items())]}

        if self.locked:
            self._log.warning("Cannot build in a locked directory")
            return
        # create lockfile
        self._log.debug("Creating lock file '%s'", self._lockFile)
        open(self._lockFile, "w").write(
            "{pid} - {started}\n".format(**metadata))

        try:
            # Build the documentation in a temporary directory.
            # - prepare the temporary directory
            import getpass
            import tempfile
            username = getpass.getuser()
            tempdirs = filter(os.path.isdir,
                              [os.path.join(os.path.sep, "build", "tmp"),
                               os.path.join(
                                   os.path.sep, "build", username, "tmp"),
                               os.path.join(os.path.sep, "build", username)])
            if tempdirs:
                tempdir = tempfile.mkdtemp("doxygen", dir=tempdirs[0])
            else:
                tempdir = tempfile.mkdtemp("doxygen")

            # use a subdirectory of the tempdir for each of C++ and Python
            if self._hasCpp():
                cpptempdir = os.path.join(tempdir, "cpp")
                os.makedirs(cpptempdir)
            pytempdir = os.path.join(tempdir, "py")
            os.makedirs(pytempdir)
            # this will be created afterwards
            deploytempdir = os.path.join(tempdir, self.name)

            self._generateDoxyConf(doxygen_versions)

            self._log.info("Running doxygen")
            self._log.debug(_which("doxygen"))
            # - modify the doxygen file to use a temporary directory

            if self._hasCpp():
                self._buildCpp(cpptempdir, doxygen_versions[0])
                shutil.copy(os.path.join(cpptempdir, 'DoxyWarnings.log'),
                            os.path.join(self.path, 'DoxyWarningsCpp.log'))
            self._buildPy(pytempdir, doxygen_versions[1])
            shutil.copy(os.path.join(pytempdir, 'DoxyWarnings.log'),
                        os.path.join(self.path, 'DoxyWarningsPy.log'))

            metadata['completed'] = datetime.now().strftime(
                self._docLockTimeFmt)

            # copy the documentation from the temporary directory to the final
            # place with a temporary name
            self._log.info("Copy generated files from temporary directory")
            try:
                # prepare the diretory to be published
                self._log.debug('prepare temporary deployment directory')
                if self._hasCpp():
                    # Use C++ doc as top level
                    os.rename(os.path.join(cpptempdir, 'html'), deploytempdir)
                    os.rename(os.path.join(pytempdir, "html"),
                              os.path.join(deploytempdir, 'py'))
                else:
                    # Use Python doc as top level
                    os.rename(os.path.join(pytempdir, 'html'), deploytempdir)

                # copy files to the doxygen directory (dependency graph,
                # class.php)
                for f in [f for f in os.listdir(os.path.join(self.path, "conf"))
                          if f.startswith("dependencies.") or f in ["class.php"]]:
                    src = os.path.join(self.path, "conf", f)
                    shutil.copyfile(src, os.path.join(deploytempdir, f))
                    if self._hasCpp():  # we need to copy the graphs also in the Python directory
                        shutil.copyfile(src,
                                        os.path.join(deploytempdir, "py", f))

                # add doc metadata to the dir
                with open(os.path.join(deploytempdir, 'info.json'),
                          'wb') as md_file:
                    json.dump(metadata, md_file, indent=2, sort_keys=True)

                self._log.debug('compress the deployment dir with zip')
                call(['zip', '-r', '-q', self.name + '.zip', self.name],
                     cwd=tempdir)

                self._log.debug('copy zip to %s', self.zipname)
                if os.path.exists(self.zipname + '.bk'):
                    os.remove(self.zipname + '.bk')
                if os.path.exists(self.zipname):
                    os.rename(self.zipname, self.zipname + '.bk')
                # For some reason "eos cp" fails while copy via eosmount works
                # call(['eos', 'cp', deploytempdir + '.zip', self.zipname])
                shutil.copy(deploytempdir + '.zip', self.zipname)
                # extract the special files needed for Doxygen search
                call(['unzip', '-q', '-o', self.zipname] +
                     [os.path.join(self.name, f)
                      for f in ['search.php',
                                'search_config.php',
                                'search_functions.php',
                                'search_opensearch.php',
                                'search/search.idx',
                               ]],
                     cwd=self.root)

                self._updateDocsDB()
                self._log.debug("Documentation ready")

            except shutil.Error, exc:
                self._log.error("Failed to copy files: %s", exc)
                with open(os.path.join(self.path, 'shutil_error.txt'), 'w') as error_log:
                    error_log.write(str(exc))

            # Mark as built
            self.toBeBuilt = False
            # Since we have built the doc, remove the "rebuild flag" if present
            if os.path.exists(self._rebuildFlagFile):
                os.remove(self._rebuildFlagFile)

            shutil.rmtree(tempdir)

        finally:
            # clean up the lock
            self._log.debug("Removing lock file '%s'", self._lockFile)
            os.remove(self._lockFile)

    def _updateDocsDB(self):
        """
        Update the links in the common directory.
        """
        db = DocsDB(self.root)
        for project, version in self.projects.items():
            db.add(project, version, self.name)
        db.write()

    def __len__(self):
        """
        Returns the number of projects hosted.
        """
        return len(self.projects)

    def __cmp__(self, other):
        """
        Comparison operator.
        """
        return cmp((len(self), self.name), (len(other), other.name))


#--- Application logic


def makeDocs(projects, root, no_build=False,
             doxygen_versions=(None, None)):
    """
    @param projects: list of pairs with (name, version) for each project to use
    @param root: base directory for the documentation directories
    @param no_build: flag to prevent the execution of Doxygen (for testing)
    @param doxygen_versions: pair of strings defining the version of doxygen to
                             be used for C++ and Python
    """
    if "PWD" in os.environ:
        # This is needed because PWD is not updated by Popen and cmt gets confused
        # if it is set
        del os.environ["PWD"]

    docs = Doc.all(root)

    # keep only projects that are not already known
    def projectNotKnown(pv):
        'tell if a project is not known'
        project, version = pv
        for d in docs:
            if d.getVersion(project) == version:
                logging.debug("Project %s %s is already in %s",
                              project, version, d.name)
                return False
        logging.info("doc missing for %s %s", project, version)
        return True
    projects = set(filter(projectNotKnown, projects))

    # list to keep track of the projects that have been added as
    # dependencies of others
    projects_added = set()
    for project, version in projects:
        logging.debug('checking %s %s', project, version)
        if (project, version) in projects_added:
            logging.debug('already added (as dependency)')
            continue
        # Get all the dependencies of the project
        # (projects without dependencies like LbScript will use their own doc directory)
        try:
            deps = _getProjDeps(project, version, recursive=True)
        except lookup.MissingProjectError as err:
            logging.warning('ignoring %s/%s: problem scanning deps: %s',
                            project, version, err)
            continue
        # get the candidates, i.e. the doc directories that can include the requested
        # project with its dependencies
        candidates = [d for d in docs
                      if d.canHost(project, version, deps)]
        if not candidates:
            # no candidates available, create a new doc dir
            logging.debug("Creating new doc dir")
            doc = Doc.makeNew(root)
            docs.append(doc)  # add the new doc to the list of known docs
        elif len(candidates) == 1:
            # only one candidate, use it
            doc = candidates[0]
            logging.debug("Using %s", doc.name)
        else:
            # more than one candidate
            logging.warning("More than one candidate doc dir for %s %s: %s",
                            project, version,
                            ", ".join([d.name for d in candidates]))
            # select the best candidate
            candidates.sort()
            doc = candidates[-1]
            logging.warning("Using %s", doc.name)
        # First add the dependencies (not yet there)
        for p, v in deps:
            if doc.getVersion(p) is None:
                doc.add(p, v)
        projects_added.update(deps)
        # Then add the project
        doc.add(project, version)
        projects_added.add((project, version))

    # Build all the documentations marked as to be built
    for doc in [d for d in docs if d.toBeBuilt]:
        if _has_kinit:
            # ensure that we have got a fresh token (if possible) before each
            # new build
            call(['kinit', '-R'])
        if no_build:
            # if we should not run doxygen, at least generate the doxygen
            # configuration
            doc._generateDoxyConf()
            # and create the fake sym-links
            doc._updateDocsDB()
        else:
            doc.build(doxygen_versions)

    projects -= projects_added
    if projects:
        logging.warning("%d projects not added:", len(projects))
        for project, version in sorted(projects):
            logging.warning("  - %s/%s", project, version)


def checkDocs(projects, root, timeout=None):
    """
    Check if there are no obvious problems in the documentation directories.

    @param projects: list of pairs with (name, version) for each project to use
    @param root: base directory for the documentation directories
    @param timeout: number of hours to wait befor considering a Doc directory as
           too old.
    @return: 0 in case of no problem, 1 if stale directories, 2 if unknownProjects
             3 if both stale and unknown
    """
    retval = 0

    docs = Doc.all(root)

    stale = []
    if timeout:
        # stale doc builds
        now = datetime.now()
        dt = timedelta(seconds=timeout * 60 * 60)  # 23 hours
        stale = [doc for doc in docs
                 if doc.locked and ((now - doc.locked[1]) > dt)]

    if stale:
        logging.warning("Documentation directories which are locked since "
                        "more than %d hours", timeout)
        for doc in stale:
            logging.warning("\t%s", doc)
        docs = [doc for doc in docs if doc not in set(stale)]
        retval |= 1

    # This is tricky: removes from the set of projects passed to the list
    # ('set(projects)') the projects present in any of the documentations.
    unknownProjects = reduce(lambda s, v: s.difference(v),
                             [doc.projects.items() for doc in docs],
                             set(projects))
    if unknownProjects:
        logging.warning("Projects without documentation")
        for proj, version in sorted(unknownProjects):
            logging.warning("\t%s %s", proj, version)
        retval |= 2

    return retval


def getLatestVersions(versions):
    """
    Given a list of project versions in the form of (project, version),
    return a list of the latest version for each project.
    """
    from LbConfiguration.Version import CoreVersion
    projects = {}  # dictionary with a list of versions per project
    for p, v in [(p.lower(),      # ensure we use the lowercase name
                  CoreVersion(v))  # Sortable class to wrap a version string
                 for p, v in versions
                 if CoreVersion.version_style.match(v)]:
        try:
            projects[p].append(v)
        except:
            projects[p] = [v]
    # sort the versions of each project and get the last one
    for p in projects:
        projects[p].sort()
        projects[p] = str(projects[p][-1])
    return projects.items()


def findAllProjects(exclude=None):
    """
    Find all the available (project, version) pairs available in the release area.
    """
    if exclude is None:  # default exclusion
        exclude = set(
            ["GANGA", "DIRAC", "LHCBGRID", "CURIE", "GEANT4", "COMPAT",
             "LHCBEXTERNALS"])
    else:
        exclude = set([p.upper() for p in exclude])
    search_path = filter(os.path.isdir, lookup.path)
    logging.debug("Looking for projects in %s", ':'.join(search_path))
    # for each entry in the search path, get (NAME, version) for each subdir
    # of the format NAME/NAME_version/{InstallArea,manifest.xml}, where
    # version is in the format vXrY[pZ]
    projects = set()
    for path in search_path:
        for project in os.listdir(path):
            projdir = os.path.join(path, project)
            if project in exclude or not os.path.isdir(projdir):
                continue
            for version, vdir in [(vdir.split('_', 1)[1],
                                   os.path.join(projdir, vdir))
                                  for vdir in os.listdir(projdir)
                                  if vdir.startswith(project + '_v')
                                      and os.path.isdir(os.path.join(projdir,
                                                                     vdir))]:
                if (not isValidVersion(project, version) or
                        not os.path.isdir(vdir)):
                    continue
                if (os.path.isdir(os.path.join(vdir, 'InstallArea')) or
                        os.path.isfile(os.path.join(vdir, 'manifest.xml'))):
                    projects.add((project, version))
    logging.debug("Found %d projects:", len(projects))
    projects = sorted(projects)
    for project, version in projects:
        logging.debug("  - %s/%s", project, version)
    return projects


def findBrokenDocs(root):
    """
    Find all broken doc directories.
    A doc directory is broken if it contains (those containing broken symlinks, i.e.
    """
    return [d for d in Doc.all(root) if d.broken]


def findUnusedDocs(root):
    """
    Find all doc directories that are not referenced from the common directory.
    """
    referenced = DocsDB(root).referenced()
    return [d for d in Doc.all(root) if d.name not in referenced]


def cleanArchivedProjects(root):
    """
    Delete from the docs directories the broken links.
    """
    logging.warning("Cleaning archived project docs from %s", root)
    broken_docs = findBrokenDocs(root)
    # Collect the list of archived projects
    archived_project = set()
    for d in broken_docs:
        for b in d.broken:
            archived_project.add(b)
            try:
                l = os.path.join(d.path, b)
                logging.info("Removing %s", l)
                os.remove(l)
            except IOError, x:  # ignore exceptions during removal
                logging.warning("IOError: %s", x)
            except OSError, x:  # ignore exceptions during removal
                logging.warning("OSError: %s", x)
    if not archived_project:
        return
    # For each archived project, remove the entry in the common directory
    # (docs/<project>/<version>)
    db = DocsDB(root)
    for p in archived_project:
        db.remove(*p.split('_'))
    db.write()


def removeUnusedDocs(root):
    """
    Delete the doc directories that are not referenced by the common doc directory.
    """
    logging.warning("Removing unused doc directory from %s", root)
    for d in findUnusedDocs(root):
        logging.warning("Removing %s", d.path)
        shutil.rmtree(d.path)


def main():
    '''
    main script logic
    '''
    from optparse import OptionParser
    parser = OptionParser(
        usage="%prog [options] project version [project/version ...]")
    parser.add_option("--verbose", action="store_true",
                      help="Print more messages")
    parser.add_option("--debug", action="store_true",
                      help="Print debugging messages")
    parser.add_option("--root", action="store",
                      help="Override the root directory of the documentation "
                           "[default: current directory]")
    parser.add_option("--no-build", action="store_true",
                      help="Do not run doxygen")
    parser.add_option("--clean-archived", action="store_true",
                      help="Remove all the links of archived projects")
    parser.add_option("--remove-unused", action="store_true",
                      help="Delete unused documentations")
    parser.add_option("-x", "--exclude", action="append",
                      help="projects to ignore")
    parser.add_option("--doxygen-version", action="store",
                      help="Version of doxygen to use. Note: it works only "
                      "when using the LbScript doxygen wrapper and if "
                      "the given version exists in the LCG externals")
    parser.add_option("--doxygen-py-version", action="store",
                      help="Same as --doxygen-version, but allows to "
                      "specify a version of doxygen for the Python "
                      "documentation (the default is to use the version "
                      "specified with --doxygen-version)")
    parser.add_option("--timeout", action="store",
                      metavar="HOURS",
                      type="int",
                      help="Do not allow the script to run more than the "
                      "specified number of hours, 0 (default) means no "
                      "timeout.")
    parser.add_option("--check", action="store_true",
                      help="Check if there are problems in the Doc directories")

    parser.set_defaults(root=os.getcwd())

    opts, args = parser.parse_args()

    log_level = logging.WARNING
    if opts.verbose:
        log_level = logging.INFO
    if opts.debug:
        log_level = logging.DEBUG
    logging.basicConfig(level=log_level)

    if not args:
        projects = findAllProjects(opts.exclude)
    else:
        projects = sorted(set((p.upper(), v)
                              for p, v in [pv.split('/', 1) for pv in args]))

    if opts.check:
        sys.exit(checkDocs(projects, opts.root, opts.timeout))

    # Ensure that the version of doxygen used for Python is the same as the one
    # used for C++ unless specified differently.
    if opts.doxygen_version and not opts.doxygen_py_version:
        opts.doxygen_py_version = opts.doxygen_version

    # Clean-up functions
    if opts.clean_archived:
        cleanArchivedProjects(opts.root)
    if opts.remove_unused:
        removeUnusedDocs(opts.root)
    # Main function
    makeDocs(projects, opts.root, opts.no_build,
             doxygen_versions=(opts.doxygen_version, opts.doxygen_py_version))

if __name__ == '__main__':
    main()
