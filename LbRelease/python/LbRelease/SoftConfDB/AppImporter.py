#!/usr/bin/env python
"""
A script to add a project to the Software Configuration DB

"""
import logging
import os
import re
import sys
import urllib2
import LbUtils
import json

from LbConfiguration.Repository import getRepositories
from LbRelease import rcs
from LbRelease.SvnTools.Project import translateProject, getProjectCmt
from LbUtils.Processes import callCommand
from LbRelease.SoftConfDB.SoftConfDB import SoftConfDB
from LbUtils.CMake import getHeptoolsVersion, getGaudiUse

# SVN Repositories definition
url = str(getRepositories(protocol='anonymous')["lbsvn"])
lbsvn = rcs.connect(url)

gaudiurl = str(getRepositories(protocol='anonymous')["gaudi"])
gaudisvn = rcs.connect(gaudiurl)

diracurl = str(getRepositories(protocol='anonymous')["dirac"])
diracsvn = rcs.connect(diracurl)

def importerTranslateProject(p, v):
    ''' Function needed to prevent LCGCMT to be passed to translateProject
    as that does not work '''
    if p.lower() == "lcgcmt" or p.lower() == "lcg":
        return (p.upper(), v)
    else:
        return translateProject(p, v)

class GitlabProject:
    """ Helper class to manager projects hosted in gitlab """

    def __init__(self, project, version, sourceuri):

        ## SourceURI like: gitlab-cern:LHCb-SVN-mirrors/Brunel#v50r1

        self.log = logging.getLogger()
        self.project = project.upper()
        self.version = version

        # Hack alert: This should NOT be here, we need to find a correct location
        # for all the  location for the constants for CERN gitlab...
        self.gitlabHTTPSURL =  "https://gitlab.cern.ch"
        self.gitlabSSHURL = "ssh://git@gitlab.cern.ch:7999"
        self.gitlabViewURL = "https://gitlab.cern.ch"

        from urlparse import  urlsplit
        self.sourceuri = sourceuri
        url = urlsplit(sourceuri)
        self.scheme = url.scheme
        self.path = url.path

        tmp = self.path.split("/")
        if len(tmp) != 2:
            raise Exception("Path without Group/Name: %s" % self.path)

        self.gitlabGroup = tmp[0]
        self.gitlabName = tmp[1]


    def getURL(self, file=None):
        """ Returns the URL at which files can be found in gitlab
        e.g. https://gitlab.cern.ch/gaudi/Gaudi/raw/v27r0/CMakeLists.txt
        """
        prefix =  self.gitlabViewURL + "/" \
                 + self.gitlabGroup + "/" + self.gitlabName + "/raw/" \
                 + self.version
        if file != None:
            return prefix + "/" + file
        else:
            return prefix

    def getToolchain(self):
        self.toolchainurl = self.getURL("toolchain.cmake")
        self.log.debug("Getting: %s" %self.toolchainurl)
        response = urllib2.urlopen(self.toolchainurl)
        data = response.read()
        self.log.debug("Got: %s" % self.toolchainurl)
        return data

    def getCMakeLists(self):
        self.cmakelistsurl = self.getURL("CMakeLists.txt")
        self.log.debug("Getting: %s" % self.cmakelistsurl)
        try:
            response = urllib2.urlopen(self.cmakelistsurl)
            data = response.read()
            self.log.debug("Got: %s" % self.cmakelistsurl)
        except:
            self.log.warning("Could not find CMakeLists at: %s" %  self.cmakelistsurl)
            raise            
        return data

    def getProjectCMT(self):
        self.projectcmturl = self.getURL("cmt/project.cmt")
        self.log.debug("Getting: %s" % self.projectcmturl)
        try:
            response = urllib2.urlopen(self.projectcmturl)
            data = response.read()
            self.log.debug("Got: %s" % self.projectcmturl)
        except:
            self.log.warning("Could not find Project.cmt at: %s" %  self.projectcmturl)
            raise            
        return data


    def getProjectConfig(self):
        """ URL folows pattern:
        https://gitlab.cern.ch/lhcb-dirac/LHCbDIRAC/raw/v8r6p3/dist-tools/projectConfig.json
        """
        self.projconfigurl = self.getURL("dist-tools/projectConfig.json")
        self.log.debug("Getting: %s" % self.projconfigurl)
        try:
            response = urllib2.urlopen(self.projconfigurl)
            data = response.read()
            self.log.debug("Got: %s" % self.projconfigurl)
        except:
            self.log.warning("Could not find projectConfig.json at: %s" %  self.projconfigurl)
            raise
        return data

    def getDepsFromProjectConfig(self, data):
        pc = json.loads(data)
        used_projects = pc["used_projects"]
        deps = []
        for l in used_projects["project"]:
            deps.append((l[0], l[1]))
        return deps

    def getDepsFromProjectCMT(self, data):
        deps = []
        for l in data.splitlines():
            m = re.match("\s*use\s+(\w+)\s+([\w\*]+)", l)
            if m != None:
                dp = m.group(1)
                dv = m.group(2)
                # removing the project name from the version if there
                dv = dv.replace(dp + "_", "")
                deps.append((dp, dv))
        return deps

    def getDependencies(self):
        """ Returns the list of project dependencies """
        pupper = self.project.upper()
        print pupper
        if pupper in [ "GAUDI",  "GEANT4"]:
            # For GAUDI we take the dependency in the toolchain file
            data = self.getToolchain()
            htv = getHeptoolsVersion(data)
            deplist = [ ("LCG", htv) ]
            return deplist
        if pupper in [ "DIRAC", "LHCBGRID" ]:
            return []
        else:
            # For all other projects use the gaudi_project macro
            # First we try to find teh CMakeLists
            # Second we try the projectConfig.json
            # Third we try the project.cmt for legacy projects
            try:
                self.log.warning("Looking for CMakeLists.txt")
                data = self.getCMakeLists()
                deplist = getGaudiUse(data)
            except:
                try:
                    self.log.warning("Looking for projectConfig.json")
                    data = self.getProjectConfig()
                    deplist = self.getDepsFromProjectConfig(data)
                    return deplist
                except:
                    try:
                        self.log.warning("Looking for legacy CMT project.cmt")
                        data = self.getProjectCMT()
                        deplist = self.getDepsFromProjectCMT(data)
                        return deplist
                    except:
                        self.log.error("Could not find project dependency metadata")
                        raise Exception("Could not find project metadata")
        return []


class AppImporter:
    """ Tool to add new project/version to the Software configuration
    DB from the version control systems """

    def __init__(self, autorelease = True):
        # Creating the SoftConfDB Object
        self.mConfDB = SoftConfDB()
        self.log = logging.getLogger()
        self.installArea = None
        self.mAutorelease = autorelease

    def inGitlab(self, project, alturi=None):
        """ Check whether the project is handled in GIT or SVN """
        sourceuri = None
        if alturi == None:
            props = self.mConfDB.getProjectProperties(project.upper())
            if props != None and "sourceuri" in props.keys():
                sourceuri = props["sourceuri"]
        else:
            sourceuri = alturi

        from urlparse import urlsplit
        if sourceuri != None:
            if urlsplit(sourceuri).scheme == "gitlab-cern":
                return True
        else:
            return False

    def gitlabProcessProjectVersion(self, p, v, alreadyDone = [], recreate=False,
                                    alturi=None, saveURIinPV=False):
        """ Get the dependencies for a single project """
        # Cleanup the project name and version and get the SVN URL
        (proj,ver)=importerTranslateProject(p,v)

        # Getting the project properties and locating the CMakeLists
        props = self.mConfDB.getProjectProperties(proj.upper())
        gp = GitlabProject(proj, ver, alturi)
        deps =  gp.getDependencies()

        # Formatting the project name/version
        corver = ver
        if proj in ver:
            corver = ver.replace(proj + "_", "")
        proj = proj.upper()

        # Looking for the project version in the DB
        tmp = self.mConfDB.findVersion(proj, ver)
        createNode = False
        node_parent = None

        # First checking if the node is there with the correct revision
        if len(tmp) != 0:
            node = tmp[0][0]
            node_parent = node
            # Need to add commit to the DB
        #If the node does not exist just create it...
        else:
            createNode = True

        if createNode:
            self.log.warning("Creating project %s %s" % (proj, ver))
            node_parent = self.mConfDB.getOrCreatePV(proj, corver)
            if saveURIinPV:
                self.log.warning("Setting sourceuri=%s for %s %s" % (gp.sourceuri, proj, ver))
                self.mConfDB.setPVProperty(proj, corver, "sourceuri", gp.sourceuri)
            # If releasing is needed!
            if self.mAutorelease and proj.upper() not in [ "LCG", "LCGCMT"]:
                self.log.warning("Requesting release of %s %s" % (proj, corver))
                self.mConfDB.setReleaseFlag(proj, corver)

        if len(deps) == 0:
            self.log.warning("No dependencies found for %s %s" % (p, v))
            return node_parent

        # Now creating the dependencies
        for (dp, dv) in deps:
            if dp in dv:
                dv = dv.replace(dp + "_", "")
            dp = dp.upper()
            self.log.warning("Find project %s %s" % (dp, dv))
            node_child = self.processProjectVersion(dp, dv, alreadyDone, recreate)

            # Now checking if the links exist
            if self.mConfDB.nodesHaveRelationship(node_parent, node_child, "REQUIRES"):
                self.log.warning("Pre-existing dependency (%s, %s)-[:REQUIRES]->(%s, %s)" % (proj, ver, dp, dv))
            else:
                self.log.warning("Adding dependency (%s, %s)-[:REQUIRES]->(%s, %s)" % (proj, ver, dp, dv))
                self.mConfDB.addRequires(node_parent, node_child)

        return node_parent


    ##
    # Main entry point for the importer
    #
    def processProjectVersion(self, p, v, alreadyDone = [], recreate=False, sourceuri=None):
        """ Get the dependencies for a single project """
        # Cleanup the project name and version and get the SVN URL
        (proj,ver)=importerTranslateProject(p,v)
        tagpath = ""

        # If not forced check in the DB what the source URI should be
        forcedSourceURI = False
        if sourceuri == None:
            sourceuri = self.mConfDB.getSourceURI(p, v)
        else:
            forcedSourceURI = True
            # Only in this case do we need to save it in the project/version node

        # Now checking whether we should get the infor from SVN of GIT
        gitlab = self.inGitlab(proj, alturi=sourceuri)

        if gitlab:
            # In this case use the new code
            # This should be cleanup up but in the transition period
            # we'll use this hack
            self.log.warning("Project %s is in Gitlab URI:%s" % (proj, sourceuri))
            return self.gitlabProcessProjectVersion(p, v, alturi=sourceuri, saveURIinPV=forcedSourceURI)
        # Getting the project.cmt file with dependencies
        if proj.upper() == "GANGA":
            projcmt = self.getGangaProjectCMT(ver)
        else:
            if proj.upper() == "GAUDI":
                tagpath = gaudisvn.url(proj,ver, isProject=True)
            elif proj.upper() == "LHCBDIRAC" or proj.upper() == "DIRAC":
                tagpath = diracsvn.url(proj,ver, isProject=True)
            else:
                tagpath=lbsvn.url(proj,ver, isProject=True)
            self.log.debug("SVN PATH:" + tagpath)
            # Get the project.cmt file and parse it to retrieve the dependencies
            #if not tagpath.endswith("cmt"):
            #    tagpath += "/cmt"
            projcmt=getProjectCmt(tagpath).strip()
            self.log.debug("Project tag SVN path:" + tagpath)
        deps = []

        # Formatting the project name/version
        corver = ver
        if proj in ver:
            corver = ver.replace(proj + "_", "")
        proj = proj.upper()

        # Looking for the project version in the DB
        tmp = self.mConfDB.findVersion(proj, ver)
        rev = getPathLastRev(tagpath)
        # Check that we actually found it in SVN
        # protect all but LCG/LCGMCT
        if rev == None and proj.upper() not in ["LCG", "LCGCMT", "DIRAC", "LHCBGRID" ]:
            raise Exception("Project %s %s not found in SVN. Aborting" % (proj, ver))

        createNode = False
        node_parent = None

        # First checking if the node is there with the correct revision
        if len(tmp) != 0:
            node = tmp[0][0]
            node_parent = node
            noderev = node["Rev"]
            if noderev != None:
                self.log.warning("Project %s %s already exists with revision %s (path rev: %s)" % (proj,
                                                                                                   ver,
                                                                                                   noderev,
                                                                                                   rev))
                if rev != noderev and recreate:
                    self.log.warning("Mismatch in revisions for %s %s, recreating" % (proj, ver))
                    self.log.warning("WARNING was a retag of  %s %s done? you may need to call "
                                     " lb-sdb-deletepv <proj> <ver> and reimport if "
                                     " it is the case " % (proj, ver))

                    try:
                        self.mConfDB.deletePV(proj, corver)
                    except Exception, e:
                        self.log.error(e)
                    createNode = True


            else:
                self.log.warning("Project %s %s already exists without revision" % (proj,ver))

        #If the node does not exist just create it...
        else:
            createNode = True

        # For LCG we check we don't have a LCGCMT node already...
        if createNode and proj == "LCG":
            tmplcgcmt = self.mConfDB.findVersion("LCGCMT", corver)
            if len(tmplcgcmt) > 0:
                self.log.warning("Found LCGCMT version %s instead of LCG" % corver)
                node_parent = tmplcgcmt[0][0]
            else:
                self.log.warning("Creating project %s %s" % (proj, corver))
                node_parent = self.mConfDB.getOrCreatePV(proj, corver)

        if createNode and proj != "LCG":
            self.log.warning("Creating project %s %s revision in SVN: %s" % (proj, corver, rev))
            node_parent = self.mConfDB.getOrCreatePV(proj, corver)
            node_parent["Rev"] = rev
            # If releasing is needed!
            if self.mAutorelease and proj != "LCG" and proj != "LCGCMT":
                self.log.warning("Requesting release of %s %s" % (proj, corver))
                self.mConfDB.setReleaseFlag(proj, corver)


        self.log.warning("Now checking dependencies for %s %s" % (p, v))
        for l in projcmt.splitlines():
            m = re.match("\s*use\s+(\w+)\s+([\w\*]+)", l)
            if m != None:
                dp = m.group(1)
                dv = m.group(2)
                deps.append((dp, dv))

        if len(deps) == 0:
            self.log.warning("No dependencies found for %s %s" % (p, v))
            return node_parent

        # Now creating the dependencies
        for (dp, dv) in deps:
            if dp in dv:
                dv = dv.replace(dp + "_", "")
            dp = dp.upper()
            self.log.warning("Find project %s %s" % (dp, dv))
            node_child = self.processProjectVersion(dp, dv, alreadyDone, recreate)

            # Now checking if the links exist
            if self.mConfDB.nodesHaveRelationship(node_parent, node_child, "REQUIRES"):
                self.log.warning("Pre-existing dependency (%s, %s)-[:REQUIRES]->(%s, %s)" % (proj, ver, dp, dv))
            else:
                self.log.warning("Adding dependency (%s, %s)-[:REQUIRES]->(%s, %s)" % (proj, ver, dp, dv))
                self.mConfDB.addRequires(node_parent, node_child)

        return node_parent

    def getGangaProjectCMT(self, gangaVersion):
        """ Reads the Project.cmt file for Ganga """
        if self.installArea == None:
            raise Exception("Can only get Ganga dependencies from an install area! - Set it on the AppImporter first...")

        pname = "GANGA"
        f = open(os.path.join(self.installArea, pname, pname + "_" + gangaVersion, "cmt", "project.cmt"), "r")
        txt = f.read()
        f.close()
        return txt


def getProjectLastRev(project, version):
    ''' Get the latest revision of a project in SVN
    Allows to check for retagging '''
    (proj,ver)=importerTranslateProject(project, version)
    tagpath = ""

    if proj.upper() == "GAUDI":
        tagpath = gaudisvn.url(proj,ver, isProject=True)
    elif proj.upper() == "LHCBDIRAC" or proj.upper() == "DIRAC":
        tagpath = diracsvn.url(proj,ver, isProject=True)
    else:
        tagpath=lbsvn.url(proj,ver, isProject=True)
    return getPathLastRev(tagpath)


def getPathLastRev(path):
    ''' Check the SVN revision of the given SVN path '''
    rev = None
    infostr = callCommand('svn','info',path)[0]
    for l in infostr.splitlines():
        m = re.match("Last Changed Rev: (\d+)", l)
        if m != None:
            rev = m.group(1)
            break
    return rev
